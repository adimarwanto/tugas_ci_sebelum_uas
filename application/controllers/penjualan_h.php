<?php
defined('BASEPATH') OR Exit('No direct script access allowed');

class penjualan_h extends CI_Controller
{
	
	Public function __construct()
	{
		parent::__construct();
		//load model terkait (untuk manggil scrip pertama kali di jalankan )
		$this->load->model("penjualan_h_model");
		$this->load->model("supplier_model");
		$this->load->model("barang_model");
		$this->load->model("karyawan_model");
		$this->load->library('pdf');

		$user_login = $this->session->userdata();
		if (count($user_login)<=1) {
			# code...
			redirect("auth/index", "refresh");
		}
	}	
	public function index()
	{
		$this->listpembelian_h();
	}
	 public function listpenjualan_h()
	{

		if (isset($_POST['tombol_cari'])) {
			# code...
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_pencarian', $data['kata_pencarian']);


		}else{
			$data['kata_pencarian'] = $this->session->userdata('session_pencarian_penjualan_h');
		}
		
		//echo "<pre>";
		//print_r($this->session->userdata()); die();
		//echo "</pre>";

		//$data['data_karyawan'] = $this->karyawan_model->tampilDataKaryawan();
		$data['data_pembelian'] = $this->penjualan_h_model->tombolpagination($data['kata_pencarian']);
		// $data['data_pembelian'] = $this->penjualan_h_model->tampilDataPenjualan_h();
		// $data['data_pembelian'] = $this->karyawan_model->tampilDataKaryawan();
		$data['content']        = 'forms/new_list_penjualan';
		
		$this->load->view('new_home', $data); 
	}
	public function cariLaporan()
	{
		if (!empty($_REQUEST)) {
			# code...
			$tgl_awal = $this->input->post('tgl_awal');
			$tgl_akhir = $this->input->post('tgl_akhir');
			$data['data_cari_pembelian'] = $this->pembelian_h_model->			 	tampilDataReport1($tgl_awal, $tgl_akhir);
			$data['$tgl_awal'] = $tgl_awal;
			$data['$tgl_akhir'] = $tgl_akhir;
			$data['content']            = 'forms/laporan';
		
		$this->load->view('home1', $data); 
		}else{
			redirect("pembelian_h/listreport", "refresh");
		}
	}
	public function detailpenjualan($id_jual_h)
	{
				$data['detail_pembelian'] = $this->penjualan_h_model->detail($id_jual_h);
				$data['content']	   = 'forms/detail_penjualan_h';
		$this->load->view('home1', $data); 	
	}

	 public function laporan()
    {
        $data['content']            = 'forms/new_report_penjualan';
        $this->load->view('new_home', $data);

    }
     public function listreport()
	{
		if (!empty($_REQUEST)) {
			# code...
			$tgl_awal = $this->input->post('tgl_awal');
			$tgl_akhir = $this->input->post('tgl_akhir');
			$data['data_pembelian'] = $this->penjualan_h_model->tampilDataReport1($tgl_awal, $tgl_akhir);
			$data['tgl_awal'] = $tgl_awal;
			$data['tgl_akhir'] = $tgl_akhir;
			$data['content']	   = 'forms/new_list_report_penjualan';
		
		$this->load->view('new_home', $data); 
		}else{
			redirect("penjualan_h/laporan/", "refresh");
		}
		
	}
   

	public function inputPenjualan()
	{
		$data['data_supplier'] = $this->karyawan_model->tampilDataKaryawan();
		$data['data_barang'] = $this->barang_model->tampilDataBarang();
		$data['data_baru']	= $this->penjualan_h_model->createKodeUrut();
		$data['content']	   = 'forms/new_input_penjualan_h';
		//if(!empty($_REQUEST)){
			//$m_pembelian_h = $this->pembelian_h_model;
			//$m_pembelian_h->save();
			//panggil trans terakhir
			//$id_terakhir = $m_pembelian_h->idTransaksiTerakhir();
			//redirect("pembelian_h/inputDetail/". $id_terakhir, "refresh");
			//}

		$validation = $this->form_validation;
		$validation->set_rules($this->penjualan_h_model->rules());

		if ($validation->run()) {
			# code...
			$m_pembelian_h = $this->penjualan_h_model;
			$this->penjualan_h_model->save();
			$this->session->set_flashdata('info', '<div style="color: green">Simpan data Berhasil
				!</div>');
			$id_terakhir = $m_pembelian_h->idTransaksiTerakhir();
			redirect("penjualan_h/inputDetail/". $id_terakhir, "refresh");

		}
		$this->load->view('new_home',$data); 	
	}
	
	 public function inputDetail($id_pembelian_header)
	{
        // panggil data barang untuk kebutuhan form input
        $data['data_barang'] 		= $this->barang_model->tampilDataBarang();
        $data['id_header'] 	    	= $id_pembelian_header;
        $data['data_jual_detail'] 	= $this->penjualan_h_model->tampilDataPenjualanDetail($id_pembelian_header);
        $data['content']	   = 'forms/new_input_penjualan_d';
        // proses simpan ke pembelian detail jika ada request form
       // if (!empty($_REQUEST)) {
            //save detail
            //$this->pembelian_h_model->savePembelianDetail($id_pembelian_header);
            
            //proses update stok
            //$kode_barang  = $this->input->post('kode_barang');
            //$qty        = $this->input->post('qty');
            //$this->barang_model->updateStok($kode_barang, $qty);

			//redirect("pembelian_h/inputDetail/" . $id_pembelian_header, "refresh");
			//redirect("pembelian/inputdetail/" . $id_pembelian_header, "refresh");
        //}

        $validation = $this->form_validation;
		$validation->set_rules($this->penjualan_h_model->rules1());

		if ($validation->run()) {
			# code...
			$m_pembelian_h = $this->penjualan_h_model;
			$this->penjualan_h_model->savePenjualanDetail($id_pembelian_header);
			$this->session->set_flashdata('info', '<div style="color: green">Simpan data Berhasil
				!</div>');
			$kode_barang  = $this->input->post('kode_barang');
			// $nama_barang  = $this->input->post('nama_barang');
            $qty        = $this->input->post('qty');
            $this->barang_model->updateStok1($kode_barang, $qty);
			redirect("penjualan_h/inputDetail/" . $id_pembelian_header, "refresh");
		}
        
		//$data['content'] 		= 'forms/input_pembelian_detail';
		$this->load->view('new_home', $data);
	}

	public function cetakPdf($tgl_awal, $tgl_akhir)
	{
		// ukuran kertas
		$pdf = new FPDF ('P','mm','A4');

		//membuat Halaman Baru
		$pdf->AddPage();

		//setting jenis font fi pdf
		$pdf->SetFont('Arial','B', 16);
		$pdf->Cell(190, 20, 'Toko Jaya Abadi', 0, 1, 'C');
		$pdf->SetFont('Arial','B', 16);
		$pdf->Cell(190, 0, 'Laporan Penjualan Barang', 0, 1, 'C');
		$pdf->SetFont('Arial','B', 16);
		$pdf->Cell(120, 15, 'Dari Tanggal', 0, 0, 'C');
		$pdf->Cell(-55, 15, $tgl_awal, 0, 0, 'C');
		$pdf->Cell(95, 15, 's/d', 0, 0, 'C');
		$pdf->Cell(-55, 15, $tgl_akhir, 0, 0, 'C');

		$pdf->SetFont('Arial', 'B', 14, 'C');
		$pdf->Cell(30, 8, '', 0, 1, 'C');
		$pdf->Cell(30, 8, '', 0, 1, 'C');
		$pdf->Cell(30, 8, '', 0, 1, 'C');
		$pdf->Cell(10, 8, 'No', 1, 0, 'C');
		$pdf->Cell(35, 8, 'ID Pembelian', 1, 0, 'C');
		$pdf->Cell(35, 8, 'No Transaksi', 1, 0, 'C');
		$pdf->Cell(30, 8, 'Tangal', 1, 0, 'C');
		$pdf->Cell(20, 8, 'Barang', 1, 0, 'C');
		$pdf->Cell(20, 8, 'Qty', 1, 0, 'C');
		$pdf->Cell(40, 8, 'Jumlah', 1, 1, 'C');

		$no = 0;
		$total = 0;
		$report = $this->penjualan_h_model->tampilDataReport1($tgl_awal, $tgl_akhir);
		foreach ($report as $data ) {
			# code...
			$no++;

			$pdf->Cell(10, 8, $no, 1, 0, 'C');
			$pdf->Cell(35, 8, $data->id_jual_h, 1, 0, 'C');
			$pdf->Cell(35, 8, $data->no_trans, 1, 0, 'C');
			$pdf->Cell(30, 8, $data->tanggal, 1, 0, 'C');
			$pdf->Cell(20, 8, $data->total, 1, 0, 'C');
			$pdf->Cell(20, 8, $data->qty, 1, 0, 'C');
			$pdf->Cell(40, 8, 'Rp.'.number_format($data->jumlah), 1, 1, 'C');

			$total += $data->jumlah;

		}
		$pdf->SetFont('Arial', 'B', 14, 'C');
		$pdf->Cell(150, 8, 'Total Keseluruhan', 1, 0, 'C');
		$pdf->Cell(40, 8, 'Rp.'.number_format($total), 1, 1, 'C');
		$pdf->output();
	}
	//public function inputDetail($id_pembelian_header)
	//{
		//panggil data barang untuk kebutuhan form input
		//$data['data_barang'] = $this->barang_model->tampilDataBarang();
		//$data['id_header'] = $id_pembelian_header;
		//$data['data_pembelian_detail'] = $this->pembelian_h_model->tampilDataPembelianDetail($id_pembelian_header);

		//if (!empty($_REQUEST)) 
		//{
				# code...
			// save detail
			//$this->pembelian_h_model->savePemebelianDetail($id_pembelian_header);

			//proses update stok
			//$kd_barang =$this->input->post('kode_barang');
			//$qty =$this->input->post('qty');
			//$this->barang_model->updateStock($kd_barang, $qty);
			//redirect("pemebelian_h/inputDetail/" . $id_pembelian_header." refresh");
		//}
		//$this->load->view('input_pembelian_d',$data); 

	//}
	//public function listdetailkaryawan($nik)
	//{
				//$data['detail_karyawan'] = $this->karyawan_model->detail($nik);
		//$this->load->view('detail_k', $data); 	
	//}
	//public function edit($nik)
	//{
				//$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
				//$data['detail_karyawan'] = $this->karyawan_model->detail($nik);
			//if(!empty($_REQUEST)){
			//$m_karyawan = $this->karyawan_model;
			//$m_karyawan->update($nik);
			//redirect("karyawan/index", "refresh");
			//}
		//$this->load->view('edit_karyawan', $data); 	
	//}

}