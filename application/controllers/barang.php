<?php
defined('BASEPATH') OR Exit('No direct script access allowed');

class barang extends CI_Controller
{
	Public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("barang_model");
		$this->load->model("jenis_model");

		$this->load->library('form_validation');

		$user_login = $this->session->userdata();
		if (count($user_login)<=1) {
			# code...
			redirect("auth/index", "refresh");
		}
	}	
	public function index()
	{
		$this->listbarang();
	}
	public function listbarang()
	{

		if (isset($_POST['tombol_cari'])) {
			# code...
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_pencarian', $data['kata_pencarian']);


		}else{
			$data['kata_pencarian'] = $this->session->userdata('session_pencarian_barang');
		}
		
		//echo "<pre>";
		//print_r($this->session->userdata()); die();
		//echo "</pre>";

		//$data['data_karyawan'] = $this->karyawan_model->tampilDataKaryawan();
		


		$data['data_jenis'] = $this->jenis_model->tampilDataJenis();
		$data['data_barang'] = $this->barang_model->tombolpagination($data['kata_pencarian']);
		//$data['data_barang'] = $this->barang_model->tampilDataBarang2();
		$data['content']	   = 'forms/new_list_barang';
		$this->load->view('new_home', $data); 	 
	}
	public function inputBarang()
	{
		$data['data_jenis'] = $this->jenis_model->tampilDataJenis();
		$data['content']	   = 'forms/new_input_barang';
		$data['data_baru']	= $this->barang_model->createKodeUrut();
		//if(!empty($_REQUEST)){
			//$m_barang = $this->barang_model;
			//$m_barang->save();
			//redirect("barang/index", "refresh");
			//}

		$validation = $this->form_validation;
		$validation->set_rules($this->barang_model->rules());

		if ($validation->run()) {
			# code...

			$this->barang_model->save();
			$this->session->set_flashdata('info', '<div style="color: green">Simpan data Berhasil
				!</div>');
			redirect("barang/index", "refresh");
		}
		$this->load->view('new_home',$data); 	
		
	}
	public function detailbarang($kode_barang)
	{
				$data['data_jenis'] = $this->jenis_model->tampilDataJenis();
				$data['detail_barang'] = $this->barang_model->detail($kode_barang);
				$data['content']	   = 'forms/new_detail_barang';
		$this->load->view('new_home', $data); 	
	}
		public function edit_b($kode_barang)
	{
		
		$data['data_jenis'] = $this->jenis_model->tampilDataJenis();
		$data['detail_barang'] = $this->barang_model->editdetail($kode_barang);
		$data['content']	   = 'forms/new_edit_barang';
		//if(!empty($_REQUEST)){
			//$m_barang = $this->barang_model;
			//$m_barang->update($kode_barang);
			//redirect("barang/index", "refresh");
			//}
		$validation = $this->form_validation;
		$validation->set_rules($this->barang_model->rules());

		if ($validation->run()) {
			# code...
			$this->barang_model->update($kode_barang);
			$this->session->set_flashdata('info', '<div style="color: green">Update data Berhasil
				!</div>');
			redirect("barang/index", "refresh");
		}
		$this->load->view('new_home',$data);
	}
	public function delete($kode_barang)
	{
		$m_barang = $this->barang_model;
		$m_barang->delete($kode_barang);
		redirect("barang/index", "refresh");
	}
}
	