<?php
defined('BASEPATH') OR Exit('No direct script access allowed');

class jabatan extends CI_Controller
{
	Public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("jabatan_model");

		$user_login = $this->session->userdata();
		if (count($user_login)<=1) {
			# code...
			redirect("auth/index", "refresh");
		}
	}	
	public function index()
	{
		$this->listjabatan();
	}
	 public function listjabatan()
	{
		if (isset($_POST['tombol_cari'])) {
			# code...
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_pencarian', $data['kata_pencarian']);


		}else{
			$data['kata_pencarian'] = $this->session->userdata('session_pencarian_jabatan');
		}

		$data['data_jabatan'] = $this->jabatan_model->tombolpagination($data['kata_pencarian']);
		//$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		$data['content']	   = 'forms/new_list_jabatan';
		$this->load->view('new_home', $data); 
	}
	public function inputJabatan()
	{
				$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
				$data['content']	   = 'forms/new_input_jabatan';
				$data['data_baru']	= $this->jabatan_model->createKodeUrut();

		//if(!empty($_REQUEST)){
			//$m_jabatan = $this->jabatan_model;
			//$m_jabatan->save();
			//redirect("jabatan/index", "refresh");
			//}

		$validation = $this->form_validation;
		$validation->set_rules($this->jabatan_model->rules());

		if ($validation->run()) {
			# code...
			$this->jabatan_model->save();
			$this->session->set_flashdata('info', '<div style="color: green">Simpan data Berhasil
				!</div>');
			redirect("jabatan/index", "refresh");
		}
		$this->load->view('new_home',$data);
	}
	public function detailjabatan($kode_jabatan)
	{
			$data['detail_jabatan'] = $this->jabatan_model->detail($kode_jabatan);
			$data['content']	   = 'forms/new_detail_jabatan';
		$this->load->view('new_home', $data); 	
	}	
		public function edit_j($kode_jabatan)
	{
		
		$data['detail_jabatan'] = $this->jabatan_model->edit_jabatan($kode_jabatan);
		$data['content']	   = 'forms/new_edit_jabatan';
		//if(!empty($_REQUEST)){
			//$m_jabatan = $this->jabatan_model;
			//$m_jabatan->update($kode_jabatan);
			//redirect("jabatan/index", "refresh");
			//}

		$validation = $this->form_validation;
		$validation->set_rules($this->jabatan_model->rules());

		if ($validation->run()) {
			# code...
			$this->jabatan_model->update($kode_jabatan);
			$this->session->set_flashdata('info', '<div style="color: green">Simpan data Berhasil
				!</div>');
			redirect("jabatan/index", "refresh");
		}
		$this->load->view('new_home',$data);
	}

	public function delete($kode_jabatan)
	{
		$m_jabatan = $this->jabatan_model;
		$m_jabatan->delete($kode_jabatan);
		redirect("jabatan/index", "refresh");
	}

}