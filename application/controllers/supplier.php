<?php
defined('BASEPATH') OR Exit('No direct script access allowed');

class supplier extends CI_Controller
{
	Public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("supplier_model");

		$user_login = $this->session->userdata();
		if (count($user_login)<=1) {
			# code...
			redirect("auth/index", "refresh");
		}
	}	
	public function index()
	{
		$this->listsupplier();
	}
	 public function listsupplier()
	{
		if (isset($_POST['tombol_cari'])) {
			# code...
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_pencarian', $data['kata_pencarian']);


		}else{
			$data['kata_pencarian'] = $this->session->userdata('session_pencarian_supplier');
		}
		 		$data['data_supplier'] = $this->supplier_model->tombolpagination($data['kata_pencarian']);
		 		//$data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
		 		$data['content']	   = 'forms/new_list_supplier';
		$this->load->view('new_home', $data); 
	}
	public function inputSupplier()
	{
		$data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
		$data['content']	   = 'forms/new_input_supplier';
		$data['data_baru']	= $this->supplier_model->createKodeUrut();
		//if(!empty($_REQUEST)){
			//$m_supplier = $this->supplier_model;
			//$m_supplier->save();
			//redirect("supplier/index", "refresh");
			//}
		$validation = $this->form_validation;
		$validation->set_rules($this->supplier_model->rules());

		if ($validation->run()) {
			# code...
			$this->supplier_model->save();
			$this->session->set_flashdata('info', '<div style="color: green">Simpan data Berhasil
				!</div>');
			redirect("supplier/index", "refresh");
		}
		$this->load->view('new_home',$data); 	
	}
	public function detailsupplier($kode_supplier)
	{
				$data['detail_supplier'] = $this->supplier_model->detail($kode_supplier);
				$data['content']	   = 'forms/new_detail_supplier';
		$this->load->view('new_home', $data); 	
	}
	public function edit_s($kode_supplier)
	{
		$data['detail_supplier'] = $this->supplier_model->edit_detail($kode_supplier);
		$data['content']	   = 'forms/new_edit_supplier';
		//if(!empty($_REQUEST)){
			//$m_supplier = $this->supplier_model;
			//$m_supplier->update($kode_supplier);
			//redirect("supplier/index", "refresh");
			//}

		$validation = $this->form_validation;
		$validation->set_rules($this->supplier_model->rules());

		if ($validation->run()) {
			# code...
			$this->supplier_model->update($kode_supplier);
			$this->session->set_flashdata('info', '<div style="color: green">Simpan data Berhasil
				!</div>');
			redirect("supplier/index", "refresh");
		}
		$this->load->view('new_home',$data); 	
	}
	public function delete($kode_supplier)
	{
		$m_supplier = $this->supplier_model;
		$m_supplier->delete($kode_supplier);
		redirect("supplier/index", "refresh");
	}
}