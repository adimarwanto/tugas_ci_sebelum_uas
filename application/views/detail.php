<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Master</title>
<style type="text/css">
body ul li {
	font-family: Arial, Helvetica, sans-serif;
	font-size:16px;
	list-style-type: none;
	text-align:center;
	vertical-align:central;
	margin-left:65px;
}
body ul li ul {
	padding: 0px;
	display: none;
	text-align:left;
}
body ul li {
	background-color: #CCC;
	float: left;
	height: 40px;
	width: 150px;
	float:left;
	line-height:40px;
}
body ul li:hover ul {
	display:block;
}
body ul li:hover{
	background-color: #F00;
	top: 20px;
	right: 500px;
	bottom: 50px;
	vertical-align: central;
	text-align: center;
}

body menu {
	font-size: 10px;
}
#menu {
	background-color: #F00;
}
</style>
</head>
<body>
<table width="100%" border="0" align="center">
<tr>
    <td align="center" bgcolor="#006699"><h1>&quot;TOKO JAYA ABADI&quot;</h1></td>
  </tr>
    <td width="100" height="50" align="center" valign="middle" bgcolor="#006699"><ul id="menu" name="menu">
       	<li>Home</li>
            <li>Master
            	<ul>
                	<li><a href="<?=base_url();?>karyawan/listkaryawan">Data Karyawan</a></li>
                    <li><a href="<?=base_url();?>jabatan/listjabatan">Data Jabatan</a></li>
                    <li><a href="<?=base_url();?>barang/listbarang">Data Barang</a></li>
                    <li><a href="<?=base_url();?>supplier/listsupplier">Data Supplier</a></li>
              </ul>
        </li>
            <li>Transaksi</li>
            <li>Report</li>
            <li>Log Out</li>
    </ul></td>
  <tr>
    <td><table width="100%" border="0">
    <tr align="center">
        <td height="10"><h2>&nbsp;</h2></td>
      </tr>
      <tr align="center">
        <td height="80"><h2>Data Karyawan</h2></td>
      </tr>
      <tr bgcolor="#CCCCCC">
        <td><table width="100%" border="0">
        <?php
		 		foreach ($detail_karyawan as $data){
		?>
          <tr>
            <td align="right"><table width="100%" border="0" cellpadding="1" cellspacing="1">
              <tr>
                <td width="39%"><strong>NIK</strong></td>
                <td width="1%" align="center" valign="middle">:</td>
                <td width="60%">
                	<input name="nik" type="text" id="nik" value="<?= $nik;?>" size="120">
                </td>
              </tr>
              <tr>
                <td><strong>Nama</strong></td>
                <td align="center" valign="middle">:</td>
                <td><input name="nama" type="text" id="nama" value="<?= $nama_karyawan;?>" size="120" /></td>
              </tr>
              <tr>
                <td><strong>Tempat Lahir</strong></td>
                <td align="center" valign="middle">:</td>
                <td><input name="tempat" type="text" id="tempat" value="<?= $tempat_lahir;?>" size="120" /></td>
              </tr>
              <tr>
                <td><strong>Telp</strong></td>
                <td align="center" valign="middle">:</td>
                <td><input name="telp" type="text" id="telp" value="<?= $telp;?>" size="120" /></td>
              </tr>
              <tr>
                <td colspan="2">&nbsp;</td>
                <td align="center">
                	<input type="submit" name="simpan" value="Simpan" id="simpan">
                    &nbsp;
                    <input type="submit" name="edit" value="Edit" id="edit">
                
                </td>
                </tr>
            </table></td>
            </tr>
        </table></td>
      </tr>
      <?php } ?>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>