<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?=base_url();?>asset/admin-lte/dist/img/animasi-bergerak-bendera-indonesia-0012.gif" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url();?>karyawan/listkaryawan"><i class="fa fa-circle-o"></i> Data Karyawan</a></li>
            <li><a href="<?=base_url();?>jabatan/listjabatan"><i class="fa fa-circle-o"></i> Data Jabatan</a></li>
            <li><a href="<?=base_url();?>supplier/listsupplier"><i class="fa fa-circle-o"></i> Data Data Supplier</a></li>
            <li><a href="<?=base_url();?>barang/listbarang"><i class="fa fa-circle-o"></i> Data Barang</a></li>
            <li><a href="<?=base_url();?>jenis_barang/listjenis"><i class="fa fa-circle-o"></i> Data Jenis Barang</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Transaksi</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right"></span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url();?>pembelian_h/listpembelian_h"><i class="fa fa-circle-o"></i> Pembelian</a></li>
            <li><a href="<?=base_url();?>penjualan_h/listpenjualan_h"><i class="fa fa-circle-o"></i> Penjualan</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Report</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url();?>pembelian_h/laporan"><i class="fa fa-circle-o"></i> Report Pembelian</a></li>
            <li><a href="<?=base_url();?>penjualan_h/laporan"><i class="fa fa-circle-o"></i> Report Penjualan</a></li>
          </ul>
        </li>
        
    </section>
    <!-- /.sidebar -->
  </aside>