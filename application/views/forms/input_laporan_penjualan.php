<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>jQuery UI Datepicker - Default functionality</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker({dateFormat: "yy-mm-dd"});
     $( "#datepicker1" ).datepicker({dateFormat: "yy-mm-dd"});
  } );
  </script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('#proses').on('click', function(event){
          event.preventDefault();
          var tgl_awal = $("#datepicker1").val();
          var tgl_akhir = $("#datepicker").val();

          if (tgl_awal == "" || tgl_akhir == "") {
            alert('Tanggal Tidak Boleh Kosong');
          }else if (new Date(tgl_awal) > new Date(tgl_akhir)) {
            alert('Format Waktu Salah Input');
          }else{
            $('#forms').submit();
          }
      });
    });
  </script>
<body>
<table width="1094" border="0">
  <tr align="center" >
    <td width="1084"><?= validation_errors();?>
      <form action="<?=base_url();?>penjualan_h/listreport" method="post" name="forms" id="forms">
        <div align="center">
          <h1>Input Penjualan</h1>
        </div>
        <table width="50%" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#00FF00">
          <tr>
            <td>Tanggal Awal</td>
            <td> :</td>
            <td><input type="text" id="datepicker1" name="tgl_awal" class="form-control" placeholder="Tanggal" autocomplete="off"></td>
          </tr>
          <tr>
            <td>Tanggal Akhir</td>
            <td> :</td>
            <td><input type="text" id="datepicker" name="tgl_akhir" class="form-control" placeholder="Tanggal" autocomplete="off"></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><input type="submit" value="proses" name="proses" id="proses" /></td>
          </tr>
        </table>
      </form></td>
  </tr>
</table>
</body>
</html>