
 <section class="content">
 <!--   <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">  
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker({dateFormat: "yy-mm-dd"});
  } );
  </script> -->
      <div class="row">
        <!-- left column -->
        <div class="">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Pembelian Detail</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?= validation_errors();?>
            <form role="form" action="<?=base_url();?>pembelian_h/inputDetail/<?=$id_header;?>" method="post" enctype="multipart/form-data"name="form1" id="form1">
              <div class="box-body">
                <div class="form-group">
                  <label>Nama Barang</label>
                  <select name="kode_barang" id="kode_barang" class="form-control" value="<?= set_value('barang');?>">
                <?php foreach ($data_barang as $data){ ?>
                <option value="<?=$data->kode_barang;?>">
                  <?=$data->nama_barang; ?>
                  </option>
                <?php }?>
              </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Qty</label>
                  <input type="text" class="form-control" name="qty" id="qty" />
                </div>
                
                
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
              <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">No</th>
                  <th style="width: 100px">Kode Barang</th>
                  <th style="width: 100px">Nama Barang</th>
                  <th style="width: 40px">Qty</th>
                  <th style="width: 40px">Harga</th>
                  <th style="width: 40px">Jumlah</th>
                </tr>
                 <?php
    // var_dump($data_pembelian_detail); die();
        $no = 0;
        $total_hitung = 0;
        foreach ($data_pembelian_detail as $data) {
            $no++;
    ?>
                <tr>
              <td><?= $no; ?></td>
              <td><?= $data->kode_barang; ?></td>
              <td><?= $data->nama_barang; ?></td>
              <td><?= $data->qty; ?></td>
              <td align="right">Rp.
            <?= number_format($data->harga); ?>
            ,-</td>
            <td  align="right">Rp.
            <?= number_format($data->jumlah); ?>
            ,-</td>
              <td><a href="<?=base_url();?>pembelian_h/detailpembelian/<?=$data->id_pembelian_h;?>">
                  <button type="button" class="btn btn-primary" >Detail</button> 
            <?php 
            
    $total_hitung += $data->jumlah;
        } 
      ?>
      <tr align="center">
          <td colspan="5" align="right"><b>TOTAL</b></td>
          <td  align="right">Rp. <b>
            <?= number_format($total_hitung); ?>
          </b></td>
      <tr>
              </table>
            </div>
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
          </div>
        </div>
      <!-- /.row -->
    </section>