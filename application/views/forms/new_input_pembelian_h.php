
 <section class="content">
 <!--   <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">  
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker({dateFormat: "yy-mm-dd"});
  } );
  </script> -->
      <div class="row">
        <!-- left column -->
        <div class="">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Pembelian</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?= validation_errors();?>
            <form role="form" action="<?=base_url();?>pembelian_h/inputpembelian" method="post" enctype="multipart/form-data"name="form1" id="form1">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nomer Transaksi</label>
                  <input type="text" class="form-control" name="no_trans" id="no_trans" placeholder="Enter Nik" value="<?=$data_baru;?>" readonly="on">
                </div>
                <div class="form-group">
                  <label>Nama Supplier</label>
                  <select name="kode_supplier" id="kode_supplier" class="form-control" value="<?= set_value('supplier');?>">
                <?php foreach ($data_supplier as $data){ ?>
                <option value="<?=$data->kode_supplier;?>">
                  <?=$data->nama_supplier; ?>
                  </option>
                <?php }?>
              </select>
                </div>
                
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
          </div>
        </div>
      <!-- /.row -->
    </section>