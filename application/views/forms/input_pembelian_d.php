<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<table width="1094" border="0">
  <tr align="center" >
    <td width="1084">
      <?= validation_errors();?>
      <form action="<?=base_url();?>pembelian_h/inputDetail/<?=$id_header;?>" method="post" name="form1" id="form1">
      <div align="center">
        <h1>Input Pembelian</h1>
      </div>
      <table width="50%" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#00FF00">
        <tr>
          <td>Nama Barang</td>
          <td>:</td>
          <td><select name="kode_barang" id="kode_barang">
            <?php foreach ($data_barang as $data){ ?>
            <option value="<?=$data->kode_barang;?>">
              <?=$data->nama_barang; ?>
              </option>
            <?php }?>
          </select></td>
        </tr>
        <tr>
          <td width="42%">QTY</td>
          <td width="3%"> :</td>
          <td width="55%"><input type="text" name="qty" id="qty" maxlength="50" /></td>
        </tr>
        <tr>
          <td width="42%">Harga</td>
          <td width="3%"> :</td>
          <td width="55%"><input type="text" name="harga" id="harga" maxlength="50" /></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td><input type="submit" value="proses" name="proses" /></td>
        </tr>
      </table>
      <table width="100%" border="1">
        <tr>
          <td colspan="6"><table width="1094" border="0">
            <tr align="right" >
              <td width="1084">&nbsp;</td>
            </tr>
            <tr>
               <td width="74" align="right"><a href="<?=base_url();?>pembelian_h/listpembelian_h">[Kembali]</a></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td width="5%">No</td>
          <td width="20%">Kode Barang</td>
          <td width="24%">Nama Barang</td>
          <td width="19%">Qty</td>
          <td width="19%">Harga</td>
          <td width="32%">jumlah</td>
          <?php
    // var_dump($data_pembelian_detail); die();
        $no = 0;
        $total_hitung = 0;
        foreach ($data_pembelian_detail as $data) {
            $no++;
    ?>
        </tr>
        <tr>
          <td><?= $no; ?></td>
          <td><?= $data->kode_barang; ?></td>
          <td><?= $data->nama_barang; ?></td>
          <td><?= $data->qty; ?></td>
          <td align="right">Rp.
            <?= number_format($data->harga); ?>
            ,-</td>
          <td  align="right">Rp.
            <?= number_format($data->jumlah); ?>
            ,-</td>
          <td><a href="<?=base_url();?>barang/detailbarang/<?=$data->kode_barang;?>">Detail | </a> <a href="<?=base_url();?>barang/edit_b/<?=$data->kode_barang;?>">Edit</a></td>
        </tr>
        <?php
    $total_hitung += $data->jumlah;
   } ?>
        <tr align="center">
          <td colspan="5" align="right"><b>TOTAL</b></td>
          <td  align="right">Rp. <b>
            <?= number_format($total_hitung); ?>
          </b></td>
        </tr>
      </table>
      <p>&nbsp;</p>
    </form></td>
  </tr>
 
</table>
</body>
</html>