<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<table width="100%" border="1">
  <tr>
    <td colspan="6"><table width="1094" border="0">
      <tr align="center" >
        <td width="1084"><?= validation_errors();?>
          <form action="<?=base_url();?>barang/inputbarang" method="post" name="form1" id="form1">
            <div align="center">
              <h1>Input Data Barang</h1>
            </div>
            <table width="50%" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#00FF00">
              <tr>
                <td width="42%">Kode Barang</td>
                <td width="3%"> :</td>
                <td width="55%"><input type="text" name="kode_barang" id="kode_barang" maxlength="50" value="<?= $data_baru;?>" readonly="on"/></td>
              </tr>
              <tr>
                <td>Nama Barang</td>
                <td>:</td>
                <td><input type="text" name="nama_barang" id="nama_barang" maxlength="50" value="<?= set_value('nama_barang');?>" /></td>
              </tr>
              <tr>
                <td>Harga Barang</td>
                <td>:</td>
                <td><input type="text" name="harga_barang" id="harga_barang" maxlength="50" value="<?= set_value('harga_barang');?>" /></td>
              </tr>
              <tr>
                <td>Kode Jenis</td>
                <td>:</td>
                <td><select name="kode_jenis" id="kode_jenis" value="<?= set_value('jenis_barang');?>">
                  <?php foreach ($data_jenis as $data){ ?>
                  <option value="<?=$data->kode_jenis;?>">
                    <?=$data->nama_jenis; ?>
                    </option>
                  <?php }?>
                </select></td>
              </tr>
              <tr>
                <td>stock</td>
                <td>:</td>
                <td><input type="text" name="stock" id="stock" maxlength="50" value="<?= set_value('stock');?>" /></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><input type="submit" name="Submit" id="Submit" value="Simpan" />
                  <input type="reset" name="reset" id="reset" value="Reset" /></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><a href="<?=base_url();?>barang/listbarang">
                  <input type="button" name="kembali" id="kembali" value="Kembali ke Menu Sebelumnya" />
                </a></td>
              </tr>
            </table>
          </form></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>