 <section class="content">
      <div class="row">
        <div class="">
         <div class="box">
            <div class="box-header with-border">
              <a href="<?=base_url();?>supplier/inputSupplier"><button type="button" class="btn btn-primary">Input Data</button></a>
            </div>
            <!-- /.box-header -->
            <form action="<?=base_url();?>supplier/inputSupplier" method="POST">
            <tr align="right" >
                  <td width="1084">
                  <input type="text" name="caridata"  id="cari" placeholder="Cari Nama" />
                  <input type="submit" name="tombol_cari" id="tombol_cari" value="cari data" />
                  </td>
                </tr>
                <?php
                    if ($this->session->flashdata('info')==true) {
                      # code...
                      echo $this->session->flashdata('info');
                    }
                  ?>
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">No</th>
                  <th style="width: 100px">Kode Supplier</th>
                  <th style="width: 100px">Nama supplier</th>
                  <th style="width: 40px">Alamat</th>
                  <th style="width: 40px">Telepon</th>
                  <th style="width: 40px">Aksi</th>
                </tr>
                <?php
              $data_posisi = $this->uri->segment(4);
              $no = $data_posisi;
                if ($data_supplier != ""){
                foreach($data_supplier as $data) {
                  $no++;
                ?>
                <tr>
              <td><?= $no; ?></td>
              <td><?= $data->kode_supplier; ?></td>
              <td><?= $data->nama_supplier; ?></td>
              <td><?= $data->alamat; ?></td>
              <td><?= $data->telp; ?></td>
              <td><a href="<?=base_url();?>supplier/detailsupplier/<?=$data->kode_supplier;?>">
                  <button type="button" class="btn btn-primary" >Detail</button> 
                  <a href="<?=base_url();?>supplier/edit_s/<?=$data->kode_supplier;?>">
                    <button type="button" class="btn btn-primary" >Edit</button>
                  </a>
                  <a href="<?=base_url();?>supplier/delete/<?=$data->kode_supplier;?>">
                    <button type="button" class="btn btn-primary" >Delete</button></a></td>
            </tr>
            <?php 
        } 
      ?>
      <tr>
        <td colspan="7" align="center"><b>Halaman : </b> <?= $this->pagination->create_links(); ?> </td>
      </tr>
    <?php 
      }else{ 
    ?>
      <tr>
        <td colspan="7" align="center"><b>Data Tidak Ada </b>  </td>
      </tr>
    <?php 
      } 
    ?>
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
              </ul>
            </div>
          </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </form>
    </section>