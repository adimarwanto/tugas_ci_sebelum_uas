
 <section class="content">
 <!--   <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">  
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker({dateFormat: "yy-mm-dd"});
  } );
  </script> -->
      <div class="row">
        <!-- left column -->
        <div class="">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Jenis Barang</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?= validation_errors();?>
      <?php
foreach ($detail_jenis as $data){
  $kode_jenis =$data->kode_jenis;
  $nama_jenis=$data->nama_jenis;
}
?>
            <form role="form" action="<?=base_url();?>jenis_barang/edit_j/<?=$kode_jenis;?>" method="post" enctype="multipart/form-data"name="form1" id="form1">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Kode Jenis</label>
                  <input type="text" class="form-control" name="kode_jenis" id="kode_jenis" placeholder="Enter Nik" value="<?= $kode_jenis;?>" readonly="on">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Nama Jenis</label>
                  <input type="text" class="form-control"name="nama_jenis" id="nama_jenis" maxlength="50" value="<?= $nama_jenis;?>">
                </div>
                
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
               <a href="<?=base_url();?>jenis_barang/listjenis">
                <button type="button" class="btn btn-primary" name="kembali" id="kembali">Kembali</button>
              </div>
            </form>
            <?php  ?>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
          </div>
        </div>
      <!-- /.row -->
    </section>