 <section class="content">
      <div class="row">
        <div class="">
         <div class="box">
            <!-- /.box-header -->
            <form action="<?=base_url();?>pembelian_h/listreport" method="POST">
              <div align="center">
      <b align="center">Laporan Pembelian</b></br>
      <b align="center"><?=$tgl_awal;?> & <?=$tgl_akhir;?></b>

    </div>
    <tr>
     
     <td width="74" align="left"><a href="<?=base_url();?>pembelian_h/Laporan">[Kembali]</a></td>
     <td align="Right"><a href="<?=base_url();?>pembelian_h/cetakPdf/<?=$tgl_awal;?>/<?=$tgl_akhir;?>">[Cetak PDF]</a></td>
    
    
</tr> 

                <?php
                    if ($this->session->flashdata('info')==true) {
                      # code...
                      echo $this->session->flashdata('info');
                    }
                  ?>
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">No</th>
                  <th style="width: 100px">Id Pembeli</th>
                  <th style="width: 100px">No transaksi</th>
                  <th style="width: 40px">Tanggal</th>
                  <th style="width: 40px">Total Barang</th>
                  <th style="width: 40px">Total Qty</th>
                  <th style="width: 40px">Jumlah</th>
                </tr>
                 <?php
    // var_dump($data_pembelian_detail); die();
        $no = 0;
        $total_pembelian = 0;
        foreach ($data_pembelian as $data) {
            $no++;
    ?>
                <tr>
              <td><?= $no; ?></td>
              <td><?= $data->id_pembelian_h; ?></td>
              <td><?= $data->no_trans; ?></td>
              <td><?= $data->tanggal; ?></td>
              <td><?= $data->total; ?></td>
              <td><?= $data->qty; ?></td>
              <td><?=number_format($data->jumlah) ; ?></td>
            </tr>
            <?php 
    $total_pembelian += $data->jumlah;
  } 

  ?>
              </table>
              <table width="100%" border="1" cellspacing="5" cellpadding="5">
                <tr>
                  <td width="69%">Total Keseluruhan</td>
                  <td width="20%">Rp. <?=number_format($total_pembelian);?></td>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </form>
    </section>