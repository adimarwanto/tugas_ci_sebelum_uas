<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">  
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker({dateFormat: "yy-mm-dd"});
  } );
  </script>
</head>

<body>
<table width="100%" border="1">
  <tr>
    <td colspan="6"><table width="1094" border="0">
      <tr align="center" >
        <td width="1084">
          <?= validation_errors();?>
          <form action="<?=base_url();?>karyawan/inputkaryawan" method="post" enctype="multipart/form-data"name="form1" id="form1">
          <div align="center">
            <h1>Input Data Karyawan</h1>
          </div>
          <table width="50%" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#00FF00">
            <tr>
              <td width="42%">NIK</td>
              <td width="3%"> :</td>
              <td width="55%"><input type="text" name="nik" id="nik" maxlength="50" value="<?=$data_baru;?>" readonly="on"/></td>
            </tr>
            <tr>
              <td>Nama</td>
              <td>:</td>
              <td><input type="text" name="nama_karyawan" id="nama_karyawan" maxlength="50" /></td>
            </tr>
            <tr>
              <td>Tempat Lahir</td>
              <td>:</td>
              <td><input type="text" name="tempat_lahir" id="tempat_lahir" maxlength="50" /></td>
            </tr>
            <tr>
              <td>Jenis Kelamin</td>
              <td>:</td>
              <td><select name="jenis_kelamin" id="jenis_kelamin">
                <option value="P">Perempuan</option>
                <option value="L">Laki-Laki</option>
              </select></td>
            </tr>
            <tr>
              <td>Tanggal Lahir</td>
              <td>:</td>
              <td><input type="text" id="datepicker" name="tgl_lahir" class="form-control" placeholder="Tanggal" autocomplete="off"></td>
            </tr>
            <tr>
              <td>Telepon</td>
              <td>:</td>
              <td><input type="text" name="telp" id="telp" maxlength="50" /></td>
            </tr>
            <tr>
              <td>Alamat</td>
              <td>:</td>
              <td><textarea name="alamat" id="alamat" cols="45" rows="5"></textarea></td>
            </tr>
            <tr>
              <td>Jabatan</td>
              <td>:</td>
              <td><select name="kode_jabatan" id="kode_jabatan">
                <?php foreach ($data_jabatan as $data){ ?>
                <option value="<?=$data->kode_jabatan;?>">
                  <?=$data->nama_jabatan; ?>
                  </option>
                <?php }?>
              </select></td>
            </tr>
             <tr>
              <td>Upload Foto</td>
              <td>:</td>
              <td><input type="file" name="image" id="image"></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td><input type="submit" name="Submit" id="Submit" value="Simpan" />
                <input type="reset" name="reset" id="reset" value="Reset" /></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td><a href="<?=base_url();?>karyawan/listkaryawan">
                <input type="button" name="kembali" id="kembali" value="Kembali ke Menu Sebelumnya" />
              </a></td>
            </tr>
          </table>
        </form></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>