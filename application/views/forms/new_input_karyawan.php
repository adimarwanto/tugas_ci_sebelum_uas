
 <section class="content">
 
      <div class="row">
        <!-- left column -->
        <div class="">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Karyawan</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?= validation_errors();?>
            <form role="form" action="<?=base_url();?>supplier/inputSupplier" method="post" enctype="multipart/form-data"name="form1" id="form1">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nik</label>
                  <input type="text" class="form-control" name="nik" id="nik" placeholder="Enter Nik" value="<?=$data_baru;?>" readonly="on">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Nama Lengkap</label>
                  
                  <input type="text" class="form-control" name="nama_karyawan" id="nama_karyawan" placeholder="">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Tempat Lahir</label>
                  <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control" >
                </div>
                <div class="form-group">
                  <label>Jenis Kelamin</label>
                  <select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
                    <option value="P">Perempuan</option>
                    <option value="L">Laki-Laki</option>
                  </select>
                
                <!-- /.input group -->
              </div>
              <div class="form-group" >
                <label>Tanggal Lahir</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker" name="tgl_lahir" placeholder="Tanggal" autocomplete="off" data-date-format="yyyy-mm-dd">
                </div>
                <!-- /.input group -->
                <div class="form-group"  >
                  <label for="exampleInputPassword1">Telepon</label>
                  <input type="text" name="telp" id="telp" class="form-control" >
                </div>
                <div class="form-group">
                  <label>Alamat</label>
                  <textarea name="alamat" id="alamat" class="form-control" rows="3" placeholder="Enter ..."></textarea>
                </div>
                 <div class="form-group">
                  <label>Jabatan</label>
                  <select name="kode_jabatan" id="kode_jabatan" class="form-control" value="<?= set_value('jabatan');?>">
                <?php foreach ($data_jabatan as $data){ ?>
                <option value="<?=$data->kode_jabatan;?>">
                  <?=$data->nama_jabatan; ?>
                  </option>
                <?php }?>
              </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input type="file" name="image" id="image">

                 
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
          </div>
        </div>
      <!-- /.row -->
    </section>