
 <section class="content">
   <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">  
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker({dateFormat: "yy-mm-dd"});
  } );
  </script>
        <?= validation_errors();?>
      <?php
foreach ($detail_karyawan as $data){
  $nik  =$data->nik;
  $nama_lengkap =$data->nama_lengkap;
  $tempat_lahir =$data->tempat_lahir;
  $tgl_lahir  =$data->tgl_lahir;
  $jenis_kelamin  =$data->jenis_kelamin;
  $alamat =$data->alamat;
  $telp =$data->telp;
  $kode_jabatan =$data->kode_jabatan;
  $photo  = $data->photo;
}
?>
      <div class="row">
        <!-- left column -->
        <div class="">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Karyawan</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?=base_url();?>karyawan/edit/<?=$nik;?>" method="post" name="form1" id="form1" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="nik">Nik</label>
                  <input type="text" class="form-control" name="nik" id="nik" value="<?=$nik;?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Nama Lengkap</label>
                  <input type="text" class="form-control" name="nama_karyawan" id="nama_karyawan" value="<?= $nama_lengkap;?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Tempat Lahir</label>
                  <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control" value="<?= $tempat_lahir;?>" >
                </div>
                <div class="form-group">
                  <label>Jenis Kelamin</label>
                  <select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
                     <?php
                      $jk = ['L','P'];
                      $jk_text= ['L' => 'Laki-Laki', 'P' => 'Perempuan'];

                        foreach ($jk as $data) {
                          if ($data == $jenis_kelamin) {
                              $select = 'selected';
                          }else {
                               $select = '';
                          }?>
                    <option value="<?= $data;?>" <?=$select;?>>
                            <?=$jk_text[$data];?>
                    </option>
                          <?php } ?>
                  </select>
                <!-- /.input group -->
              </div>
                <div class="form-group">
                  <label>Tanggal Lahir</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control" id="datepicker" name="tgl_lahir"value="<?= $tgl_lahir;?>">
                </div>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Telepon</label>
                  <input type="text" name="telp" id="telp" class="form-control" value="<?= $telp;?>">
                </div>
                <div class="form-group">
                  <label>Alamat</label>
                  <textarea name="alamat" id="alamat" class="form-control" rows="3" placeholder="Enter ..."><?= $alamat;?></textarea>
                </div>
                 <div class="form-group">
                  <label>Jabatan</label>
                   <select name="kode_jabatan" id="kode_jabatan" value="<?= set_value('jabatan');?>" class="form-control">
                 <?php foreach ($data_jabatan as $data){ 
          $select = ($data->kode_jabatan == $kode_jabatan) ? 'selected' : '';
          ?>
              <option value="<?=$data->kode_jabatan;?>" <?=$select;?>>
                <?=$data->nama_jabatan; ?>
                </option>
              <?php }?>
              </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input type="text" name="image" id="image" value="<?= $photo;?>" class="form-control">

                 
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                 <a href="<?=base_url();?>karyawan/listkaryawan">
                <button type="button" class="btn btn-primary" name="kembali" id="kembali">Kembali</button>
              </a>
              </div>
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
          </div>
        </div>
      <!-- /.row -->
    </section>