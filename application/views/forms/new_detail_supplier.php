
 <section class="content">
 <!--   <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">  
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker({dateFormat: "yy-mm-dd"});
  } );
  </script> -->
      <div class="row">
        <!-- left column -->
        <div class="">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Karyawan</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
        <?= validation_errors();?>
        <?php
foreach ($detail_supplier as $data){
  $kode_supplier  =$data->kode_supplier;
  $nama_supplier  =$data->nama_supplier;
  $alamat =$data->alamat;
  $telp =$data->telp;
?>
            <form role="form" action="<?=base_url();?>supplier/edit_s/<?=$kode_supplier;?>" method="post" enctype="multipart/form-data"name="form1" id="form1">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Kode Supplier</label>
                  <input type="text" class="form-control" name="kode_supplier" id="kode_supplier" placeholder="Enter Nik" value="<?=$kode_supplier;?>" readonly="on">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Nama Supplier</label>
                  <input type="text" class="form-control"name="nama_supplier" id="nama_supplier" maxlength="50" value="<?=$nama_supplier;?>">
                </div>
                <div class="form-group">
                  <label>Alamat</label>
                  <textarea name="alamat" id="alamat" class="form-control" rows="3" placeholder="Enter ..."><?=$alamat;?></textarea>
                </div>
                <div class="form-group">
                  <label>Telepon</label>
                  <input type="text"  name="telp" id="telp" maxlength="50" value="<?=$telp;?>" class="form-control" >
                </div>
                
              <!-- /.box-body -->

                <a href="<?=base_url();?>supplier/listsupplier">
                <button type="button" class="btn btn-primary" name="kembali" id="kembali">Kembali</button>
              </a>
              </div>
            </form>
            <?php } ?>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
          </div>
        </div>
      <!-- /.row -->
    </section>