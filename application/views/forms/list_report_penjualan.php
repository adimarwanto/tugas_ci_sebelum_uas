<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
  <form action="<?=base_url();?>pembelian/listreport" method="post" name="form1" id="form1">
    <div>
      <b>Laporan Pembelian</b></br>
      <b><?=$tgl_awal;?> & <?=$tgl_akhir;?></b>

    </div>
    <tr>
     
     <td width="74" align="left"><a href="<?=base_url();?>penjualan_h/Laporan">[Kembali]</a></td>
     <td align="Right"><a href="<?=base_url();?>penjualan_h/cetakPdf/<?=$tgl_awal;?>/<?=$tgl_akhir;?>">[Cetak PDF]</a></td>
    
    
</tr> 
<table width="100%" border="1" >
 
  <td width="7%" scope="col">No</td>
    <td width="10%" scope="col">ID Pembelian</td>
    <td width="18%" scope="col">No Transaksi</td>
    <td width="13%" scope="col">Tanggal</td>
    <td width="11%" scope="col">Total Barang</td>
    <td width="10%" scope="col">Total Qty</td>
    <td width="20%" scope="col">Jumlah</td>
  </tr>
  <?php
    // var_dump($data_pembelian_detail); die();
        $no = 0;
        $total_pembelian = 0;
        foreach ($data_pembelian as $data) {
            $no++;
    ?>
  <tr>
    <td scope="row"><?= $no; ?></td>
    <td><?= $data->id_jual_h; ?></td>
    <td><?= $data->no_trans; ?></td>
    <td><?= $data->tanggal; ?></td>
    <td><?= $data->total ; ?></td>
    <td><?= $data->qty; ?></td>
    <td><?=number_format($data->jumlah) ; ?></td>
  </tr>
  <?php 
    $total_pembelian += $data->jumlah;
  } 

  ?> 
</table>
<table width="100%" border="1" cellspacing="5" cellpadding="5">
  <tr>
    <td width="69%">Total Keseluruhan</td>
    <td width="20%">Rp. <?=number_format($total_pembelian);?></td>
  </tr>
</table>
</body>
</form>
</html>