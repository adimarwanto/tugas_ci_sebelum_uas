
 <section class="content">
 <!--   <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">  
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker({dateFormat: "yy-mm-dd"});
  } );
  </script> -->
      <div class="row">
        <!-- left column -->
        <div class="">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Karyawan</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?= validation_errors();?>
            <?php
foreach ($detail_barang as $data){
  $kode_barang  =$data->kode_barang;
  $nama_barang  =$data->nama_barang;
  $harga_barang =$data->harga_barang;
  $kode_jenis   =$data->kode_jenis;
  $stock        =$data->stock;
}
?>
            <form role="form" action="<?=base_url();?>barang/edit_b/<?=$kode_barang;?>" method="post" enctype="multipart/form-data"name="form1" id="form1">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Kode Barang</label>
                  <input type="text" class="form-control" name="kode_barang" id="kode_barang" placeholder="Enter Nik" value="<?=$kode_barang;?>" readonly="on">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Nama Barang</label>
                  
                  <input type="text" class="form-control"name="nama_barang" id="nama_barang" maxlength="50" value="<?=$nama_barang;?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Harga Barang</label>
                  <input type="text"  name="harga_barang" id="harga_barang" maxlength="50" value="<?=$harga_barang;?>" class="form-control" >
                </div>
                <div class="form-group">
                  <label>Kode Jenis</label>
                  <select name="kode_jenis" id="kode_jenis" value="<?= set_value('jenis_barang');?>" class="form-control">
                 <?php foreach ($data_jenis as $data){ 
          $select = ($data->kode_jenis == $kode_jenis) ? 'selected' : '';
          ?>
              <option value="<?=$data->kode_jenis;?>" <?=$select;?>>
                <?=$data->nama_jenis; ?>
                </option>
              <?php }?>
                </select>
                
                <div class="form-group">
                  <label for="exampleInputPassword1">Stock</label>
                  <input type="text" name="stock" id="stock" maxlength="50" value="<?=$stock;?>" class="form-control" >
                </div>
                
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="<?=base_url();?>barang/listbarang">
                <button type="button" class="btn btn-primary" name="kembali" id="kembali">Kembali</button>
              </a>
              </div>
            </form>
            <?php  ?>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
          </div>
        </div>
      <!-- /.row -->
    </section>