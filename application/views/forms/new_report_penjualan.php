<div class="box box-danger">
            <div class="box-header">
              <h3 class="box-title">Input masks</h3>
            </div>
            <?= validation_errors();?>
      <form action="<?=base_url();?>penjualan_h/listreport" method="post" name="forms" id="forms">
            <div class="box-body">
              <!-- Date dd/mm/yyyy -->

              <div class="form-group">
                <label>Report Penjualan</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" id="datepicker" name="tgl_awal" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" autocomplete="off" data-date-format="yyyy-mm-dd">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- Date mm/dd/yyyy -->
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask="'alias': 'mm/dd/yyyy'" id="datepicker1" name="tgl_akhir" autocomplete="off" data-date-format="yyyy-mm-dd">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
              <div class="box-footer">
                <button type="submit" value="proses" name="proses" class="btn btn-primary">Proses</button>
              </div>
            </div>
            <!-- /.box-body -->
          </form>
          </div>