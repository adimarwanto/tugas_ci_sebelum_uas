
 <section class="content">
 <!--   <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">  
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker({dateFormat: "yy-mm-dd"});
  } );
  </script> -->
      <div class="row">
        <!-- left column -->
        <div class="">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Jabatan</h3>
            </div>
            <!-- /.box-header -->
            <?= validation_errors();?>
          <?php
foreach ($detail_jabatan as $data){
  $kode_jabatan =$data->kode_jabatan;
  $nama_jabatan =$data->nama_jabatan;
  $keterangan =$data->keterangan;
}
?>
            <!-- form start -->
            <form role="form" action="<?=base_url();?>jabatan/edit_j/<?=$kode_jabatan;?>" method="post" enctype="multipart/form-data"name="form1" id="form1">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Kode Jabatan</label>
                  <input type="text" class="form-control" name="kode_jabatan" id="kode_jabatan" placeholder="Enter Nik" value="<?=$kode_jabatan;?>" readonly="on">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Nama Jabatan</label>
                  <input type="text" class="form-control"name="nama_jabatan" id="nama_jabatan" maxlength="50" value="<?=$nama_jabatan;?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Keterangan</label>
                  <input type="text"  name="keterangan" id="keterangan" maxlength="50" value="<?=$keterangan;?>" class="form-control" >
                </div>
                
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
          </div>
        </div>
      <!-- /.row -->
    </section>