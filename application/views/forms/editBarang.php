,<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Master</title>
</head>
<body>
<table width="100%" border="0">
  <tr align="center">
    <td height="80"><h2>Edit Barang</h2></td>
  </tr>
  <tr align="center" bgcolor="#CCCCCC">
    <td>
	<div align="center" style="color:red"><?=validation_errors();?></div>
	<?php


	foreach($editBarang as $data){
		$kode_barang		=$data->kode_barang;
		$nama_barang=$data->nama_barang;
		$harga_barang=$data->harga_barang;
		$kode_jenis=$data->kode_jenis;
		$stok=$data->stok;
		}

?>
  <tr>
      <form name="form" method="POST" action="<?=base_url();?>barang/editBarang/<?=$kode_barang;?>">
        <td><table width="50%" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="">
          <tr>
            <td width="37%">Kode Barang</td>
            <td width="4%"> :</td>
            <td width="59%"><input type="text" name="kode_barang" id="kode_barang" maxlength="5" value="<?=$kode_barang?>"></td>
          </tr>
          <tr>
            <td>Nama Barang</td>
            <td>:</td>
            <td><input type="text" name="nama_barang" id="nama_barang" maxlength="150" value="<?=$nama_barang;?>"></td>
          </tr>
          <tr>
            <td>Harga Barang</td>
            <td>:</td>
            <td><input type="text" name="harga_barang" id="harga_barang" maxlength="15" value="<?=$harga_barang;?>"></td>
          </tr>
          <tr>
            <td>Kode Jenis Barang</td>
            <td>:</td>
            <td><select name="kode_jenis" id="kode_jenis">
              <?php foreach($data_jenis as $data) {
          	$select_jenis=($data->kode_jenis==$kode_jenis)?'selected':'';
           ?>
              <option value="<?=$data->kode_jenis;?>" <?=$select_jenis;?>>
                <?=$data->nama_jenis;?>
                </option>
              <?php } ?>
            </select></td>
          </tr>
          <tr>
            <td>Stok</td>
            <td>:</td>
            <td><input type="text" name="stok" id="stok" maxlength="11" value="<?=$stok;?>"></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><p>
              <input type="submit" name="Submit" id="Submit" value="Simpan">
              <input type="reset" name="reset" id="reset" value="Reset">
            </p>
            <p><a href="<?=base_url();?>barang">
              <input type="button" name="Kembali" id="Kembali" value="Kembali Ke Menu Sebelumnya">
            </a> </p></td>
          </tr>
        </table>
    </form>
</table>
</body>
</html>