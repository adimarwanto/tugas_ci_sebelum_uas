<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
.dropmenu {
    background: #616161;
    height: 30px;
    list-style-type: none;
    margin: 0;
    padding: 0px;
}
.dropmenu li {
    border-right: solid 1px white;
    float: left;
    height: 30px;
}
.dropmenu li a {
    color: #fff;
    display: block;
    font: 12px arial, verdana, sans-serif;
    font-weight: bold;
    padding: 9px 20px;
    text-decoration: none;
}
.dropmenu li:hover { background: #778899; position: relative; }
.dropmenu li:hover a { text-decoration: underline; }
.dropmenu li:hover ul {
    background-color: #3f4a54;
    border: 1px solid grey;
    left: 0px;
    padding: 3px;
    top: 30px;
    width: 160px;
}
.dropmenu li:hover ul li { border: none; height: 18px; }
.dropmenu li:hover ul li a {
    background-color: black;
    border: 1px solid transparent;
    color: #FFFFFF;
    display: block;
    font-size: 15px;
    height: 18px;
    line-height: 18px;
    padding: 0px;
    text-decoration: none;
    text-indent: 5px;
    width: 158px;
    padding: 3px;
}
.dropmenu li:hover ul li a:hover {
    background: silver;
    border: solid 1px #444;
    color: #000;
    height: 18px;
    padding: 3px;
}
.dropmenu ul {
    left: -9999px;
    list-style-type: none;
    position: absolute;
    top: -9999px;
}
</style>
</head>

<body>
<ul class="dropmenu">
<li><a href="#1">Home</a>
</li>
<li><a href="#1">Master</a>
    <ul>
    <li><a href="<?=base_url();?>karyawan/listkaryawan">Data Karyawan</a></li>
    <li><a href="<?=base_url();?>jabatan/listjabatan">Data Jabatan</a></li>
    <li><a href="<?=base_url();?>barang/listbarang">Data Barang</a></li>
    <li><a href="<?=base_url();?>supplier/listsupplier">Data Supplier</a></li>
    <li><a href="<?=base_url();?>jenis_barang/listjenis">Data Jenis Barang</a></li>
    
    </ul>
</li>
<li><a href="#2">Transaksi</a>
    <ul>
    <li><a href="<?=base_url();?>pembelian_h/listpembelian_h">Data Pembelian</a></li>
    <li><a href="<?=base_url();?>penjualan_h/listpenjualan_h">Data Penjualan</a></li>
    </ul>
</li>
<li><a href="">Report</a>
    <ul>
    <li><a href="<?=base_url();?>pembelian_h/laporan">Report Pembelian</a></li>
    <li><a href="<?=base_url();?>penjualan_h/laporan">Report Penjualan</a></li>
    </ul>
</li>
<li><a href="<?=base_url();?>auth/logout">Logout</a>
</li>
</ul>
</body>
</html>