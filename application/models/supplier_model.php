<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class supplier_model extends CI_Model
{
	//panggil nama table
	private $_table = "supplier";

	public function rules()
	{
		return
		[
			[
				'field' => 'kode_supplier',
				'label'	=> 'kode supplier',
				'rules' => 'required|max_length[5]',
				'errors' =>[
					'required' => 'kode supplier tidak Boleh Kosong',
					'max_length' => 'kode supplier tdak Boleh Lebih dari 5 karakter',
				],
			],
			[
				'field' => 'nama_supplier',
				'label'	=> 'nama supplier',
				'rules' => 'required',
				'errors' =>[
					'required' => 'nama supplier tidak Boleh Kosong',
				],
			],
			[
				'field' => 'alamat',
				'label'	=> 'alamat',
				'rules' => 'required',
				'errors' =>[
					'required' => 'alamat tidak Boleh Kosong',
				],
			],
			[
				'field' => 'telp',
				'label'	=> 'telp',
				'rules' => 'required|max_length[15]',
				'errors' =>[
					'required' => 'telp tidak Boleh Kosong',
					'max_length' => 'telp tdak Boleh Lebih dari 15 karakter',	
				],
			],
		];
	}
	
	public function tampilDataSupplier()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	}

	public function tampilDataSupplier2()
	{
		//menggunakan query
		$query = $this->db->query("SELECT * FROM supplier where flag = 1");
		return $query->result();
	}

	public function tampilDataSupplier3()
	{
		//menggunakan query 
		$this->db->select('*');
		$this->db->order_by('kode_supplier', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function save()
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabungan = $thn."-".$bln."-".$tgl;
		
		$data['kode_supplier']			= $this->input->post('kode_supplier');
		$data['nama_supplier']			= $this->input->post('nama_supplier');
		$data['alamat']					= $this->input->post('alamat');
		$data['telp']					= $this->input->post('telp');
		$data['flag']					= 1;
		$this->db->insert($this->_table, $data);
	}
	public function update($kode_supplier)
	{		
		$data['kode_supplier']			= $this->input->post('kode_supplier');
		$data['nama_supplier']			= $this->input->post('nama_supplier');
		$data['alamat']					= $this->input->post('alamat');
		$data['telp']					= $this->input->post('telp');
		$data['flag']					= 1;
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->update($this->_table, $data);
	}
	public function detail($kode_supplier)
	{
		$this->db->select('*');
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();	
	}
	public function edit_detail($kode_supplier)
	{
		$this->db->select('*');
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();	
	}
	public function tampilDataSupplierPagination($perpage, $uri, $data_pencarian)
	{
		$this->db->select('*');
		if (!empty($data_pencarian)) {
			# code...
			$this->db->like('nama_supplier', $data_pencarian);
		}
		$this->db->order_by('kode_supplier', 'asc');

		$get_data = $this->db->get($this->_table,  $perpage, $uri);
		if ($get_data->num_rows()>0) {
			# code...
			return $get_data->result();
		}else{
			return null;
		}
	}
	public function tombolpagination($data_pencarian)
	{
		$this->db->like('nama_supplier', $data_pencarian);
		$this->db->from($this->_table);
		$hasil = $this->db->count_all_results();

		$pagination['base_url']		= base_url().'supplier/listsupplier/load/';
		$pagination['total_rows']	= $hasil;
		$pagination['per_page']		= "3";
		$pagination['uri_segment']	= 4;
		$pagination['num_links']	= 2;

		$pagination['full_tag_open']	= '<div class="pagination">';
		$pagination['full_tag_close']	= '</div>';

		$pagination['first_link']		= 'First Page';
		$pagination['first_tag_open']	= '<span class="firstlink">';
		$pagination['first_tag_close']	= '</span>';

		$pagination['last_link']		= 'Last Page';
		$pagination['last_tag_open']	= '<span class="lastlink">';
		$pagination['last_tag_close']	= '</span>';

		$pagination['next_link']		= 'Next Page';
		$pagination['next_tag_open']	= '<span class="nextlink">';
		$pagination['next_tag_close']	= '</span>';

		$pagination['prev_link']		= 'Prev Page';
		$pagination['prev_tag_open']	= '<span class="prevlink">';
		$pagination['prev_tag_close']	= '</span>';

		$pagination['cur_tag_open']		= '<div class="curlink">';
		$pagination['cur_tag_close']	= '</div>';

		$pagination['num_tag_open']		= '<div class="numlink">';
		$pagination['num_tag_close']	= '</div>';

		$this->pagination->initialize($pagination);

		$hasil_pagination = $this->tampilDataSupplierPagination($pagination['per_page'],
			$this->uri->segment(4), $data_pencarian);

		return $hasil_pagination;
	}
	public function createKodeUrut()
	{
		// cek kode barang terakhir
		$this->db->select('MAX(kode_supplier) as kode_supplier');
		$query = $this->db->get($this->_table);
		$result = $query->row_array(); //hasil berbentuk array

		$kode_supplier_terakhir = $result['kode_supplier'];
		// format BR001 = BR [label] 001 [nomer urut]
		$label = 'SP';
		$no_urut = (int) substr($kode_supplier_terakhir, 2, 3);
		$no_urut ++;

		$no_urut_baru = sprintf("%03s", $no_urut);
		$kode_supplier_baru = $label . $no_urut_baru;

		// var_dump($kode_barang_baru); die();
		// var_dump(string("%03s", 0)); die();

		return $kode_supplier_baru; 
	}
	public function delete($kode_supplier)
	{
		// $this->hapusPhoto($nik);
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->delete($this->_table);
	}
	
	
}