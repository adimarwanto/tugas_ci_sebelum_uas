<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class karyawan_model extends CI_Model
{
	//panggil nama table
	private $_table = "karyawan";

	public function rules()
	{
		return
		[
			[
				'field' => 'nik',
				'label'	=> 'nik',
				'rules' => 'required|max_length[10]',
				'errors' =>[
					'required' => 'nik tidak Boleh Kosong',
					'max_length' => 'nik tdak Boleh Lebih dari 10 karakter',
				],
			],
			[
				'field' => 'nama_karyawan',
				'label'	=> 'nama karyawan',
				'rules' => 'required',
				'errors' =>[
					'required' => 'nama tidak Boleh Kosong',
				],
			],
			[
				'field' => 'tempat_lahir',
				'label'	=> 'tempat lahir',
				'rules' => 'required',
				'errors' =>[
					'required' => 'tempat lahir tidak Boleh Kosong',
				],
			],
			[
				'field' => 'jenis_kelamin',
				'label'	=> 'jenis kelamin',
				'rules' => 'required',
				'errors' =>[
					'required' => 'jenis kelamin tidak Boleh Kosong',
					
				],
			],
			[
				'field' => 'tgl_lahir',
				'label'	=> 'tanggal',
				'rules' => 'required',
				'errors' =>[
					'required' => 'Tanggal tidak Boleh Kosong',
					
				],
			],
			[
				'field' => 'telp',
				'label'	=> 'telepon',
				'rules' => 'required|max_length[15]',
				'errors' =>[
					'required' => 'telepon tidak Boleh Kosong',
					'max_length' => 'telepon tdak Boleh Lebih dari 15 karakter',
				],
			],
			[
				'field' => 'alamat',
				'label'	=> 'alamat',
				'rules' => 'required',
				'errors' =>[
					'required' => 'alamat tidak Boleh Kosong',
				],
			],
			[
				'field' => 'kode_jabatan',
				'label'	=> 'kode Jabatan',
				'rules' => 'required|max_length[5]',
				'errors' =>[
					'required' => 'kode Jabatann tidak Boleh Kosong',
					'max_length' => 'kode Jabatan tdak Boleh Lebih dari 5 karakter',
				],
			]
		];
	}
	
	public function tampilDataKaryawan()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	}

	public function tampilDataKaryawan2()
	{
		//menggunakan query
		$query = $this->db->query("SELECT * FROM karyawan as kr inner join jabatan as jb on kr.kode_jabatan = jb.kode_jabatan ");
		return $query->result();
	}

	public function tampilDataKaryawan3()
	{
		//menggunakan query 
		$this->db->select('*');
		$this->db->order_by('nik', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function save()
	{
		// $this->_set_rules();
		// if ($this->form_validation->run() == true) {
			$cek_nik = $this->db->query("SELECT * FROM karyawan where nik = " . $nik_karyawan= $this->input->post('nik') . " ")->num_rows();
			if ($cek_nik <= 0) {
		$data['nik']					= $nik_karyawan;
		$data['nama_lengkap']			= $this->input->post('nama_karyawan');
		$data['tempat_lahir']			= $this->input->post('tempat_lahir');
		$data['tgl_lahir']				= $this->input->post('tgl_lahir');
		$data['jenis_kelamin']			= $this->input->post('jenis_kelamin');
		$data['alamat']					= $this->input->post('alamat');
		$data['telp']					= $this->input->post('telp');
		$data['kode_jabatan']			= $this->input->post('kode_jabatan');
		$data['flag']					= 1;
		$data['photo']					= $this->uplodPhoto($nik_karyawan);

		// echo "<pre>";
		// print_r($this->db->insert($this->_table, $data)); die();
		// echo "</pre>";
		$this->db->insert($this->_table, $data);
			}else{
				echo "<script>alert('Nik Sudah Terdaftar')</script>";
				redirect("karyawan/inputKaryawan", "refresh");
			}
		
		

	}
	public function update($nik)
	{
		$tgl 			= $this->input->post('tgl');
		$bln 			= $this->input->post('bln');
		$thn 			= $this->input->post('thn');
		$nik_karyawan	= $this->input->post('nik');
		$tgl_gabungan 	= $thn."-".$bln."-".$tgl;
		
		$data['nik']					= $nik_karyawan;
		$data['nama_lengkap']			= $this->input->post('nama_karyawan');
		$data['tempat_lahir']			= $this->input->post('tempat_lahir');
		$data['tgl_lahir']				= $this->input->post('tgl_lahir');;
		$data['jenis_kelamin']			= $this->input->post('jenis_kelamin');
		$data['alamat']					= $this->input->post('alamat');
		$data['telp']					= $this->input->post('telp');
		$data['kode_jabatan']			= $this->input->post('kode_jabatan');
		$data['flag']					= 1;


		if (!empty($_FILES["image"]["name"])) {
			//$this->hapusPhoto($nik);
			$foto_karyawan	= $this->uplodPhoto($nik);
		}else{
			$foto_karyawan	= $this->input->post('foto_old');
		}
		$data['photo'] =$foto_karyawan;


		$this->db->where('nik', $nik);
		$this->db->update($this->_table, $data);
	}
	public function detail($nik)
	{
		$this->db->select('*');
		$this->db->where('nik', $nik);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();	
	}
	public function detail_edit($nik)
	{

		$this->db->select('*');
		$this->db->where('nik', $nik);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();	
	}
	private function uplodPhoto($nik)
	{
		$date_upload					= date('Ymd');
		$config['upload_path']			= './resources/fotokaryawan/';
		$config['allowed_types']		= 'gif|jpg|png|jpeg';
		$config['file_name']			= $date_upload . '_' . $nik;
		$config['overwrite']			= true;
		$config['max_size']				= 1024;

		//echo "<pre>";
		//print_r($_FILES["image"]); die();
		//echo "</pre>";


		$this->load->library('upload', $config);

		if ($this->upload->do_upload('image')) {
			$nama_file	= $this->upload->data("file_name");
		}else{
			$nama_file 	= "default.png";
		}
		return $nama_file;
	}
	private function hapusPhoto($nik)
	{
		$data_karyawan = $this->detail($nik);
		foreach ($data_karyawan as $data) {
			$nama_file = $data->photo;
		}

		if ($nama_file!= "default.png") {
			$path = "./resources/fotokaryawan/" . $nama_file;

			return @unlink($path);
		}
	}
	public function delete($nik)
	{
		$this->hapusPhoto($nik);
		$this->db->where('nik', $nik);
		$this->db->delete($this->_table);
	}
	public function tampilDataKaryawanPagination($perpage, $uri, $data_pencarian)
	{
		$this->db->select('*');
		if (!empty($data_pencarian)) {
			# code...
			$this->db->like('nama_lengkap', $data_pencarian);
		}
		$this->db->order_by('nik', 'asc');

		$get_data = $this->db->get($this->_table,  $perpage, $uri);
		if ($get_data->num_rows()>0) {
			# code...
			return $get_data->result();
		}else{
			return null;
		}
	}
	public function tombolpagination($data_pencarian)
	{
		$this->db->like('nama_lengkap', $data_pencarian);
		$this->db->from($this->_table);
		$hasil = $this->db->count_all_results();

		$pagination['base_url']		= base_url().'karyawan/listkaryawan/load/';
		$pagination['total_rows']	= $hasil;
		$pagination['per_page']		= "3";
		$pagination['uri_segment']	= 4;
		$pagination['num_links']	= 2;

		$pagination['full_tag_open']	= '<div class="pagination">';
		$pagination['full_tag_close']	= '</div>';

		$pagination['first_link']		= 'First Page';
		$pagination['first_tag_open']	= '<span class="firstlink">';
		$pagination['first_tag_close']	= '</span>';

		$pagination['last_link']		= 'Last Page';
		$pagination['last_tag_open']	= '<span class="lastlink">';
		$pagination['last_tag_close']	= '</span>';

		$pagination['next_link']		= 'Next Page';
		$pagination['next_tag_open']	= '<span class="nextlink">';
		$pagination['next_tag_close']	= '</span>';

		$pagination['prev_link']		= 'Prev Page';
		$pagination['prev_tag_open']	= '<span class="prevlink">';
		$pagination['prev_tag_close']	= '</span>';

		$pagination['cur_tag_open']		= '<div class="curlink">';
		$pagination['cur_tag_close']	= '</div>';

		$pagination['num_tag_open']		= '<div class="numlink">';
		$pagination['num_tag_close']	= '</div>';

		$this->pagination->initialize($pagination);

		$hasil_pagination = $this->tampilDataKaryawanPagination($pagination['per_page'],
			$this->uri->segment(4), $data_pencarian);

		return $hasil_pagination;
	}
	public function createKodeUrut()
	{
		// cek kode barang terakhir
		$this->db->select('MAX(nik) as nik');
		$query = $this->db->get($this->_table);
		$result = $query->row_array(); //hasil berbentuk array

		$nik_terakhir = $result['nik'];
		// format BR001 = BR [label] 001 [nomer urut]
		$nik_awal = date('y');
		$nik_tengah = date('m');
		$no_urut = (int) substr($nik_terakhir, 4, 3);
		$no_urut ++;

		$no_urut_baru = sprintf("%03s", $no_urut);
		$kode_barang_baru = $nik_awal . $nik_tengah . $no_urut_baru;

		// var_dump($kode_barang_baru); die();
		// var_dump(string("%03s", 0)); die();

		return $kode_barang_baru; 
	}

	
}