<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class barang_model extends CI_Model
{
	//panggil nama table
	private $_table = "barang";

	public function rules()
	{
		return[
			[
				'field' => 'kode_barang',
				'label'	=> 'kode Barang',
				'rules' => 'required|max_length[5]',
				'errors' =>[
					'required' => 'kode Barang tidak Boleh Kosong',
					'max_length' => 'kode Barang tdak Boleh Lebih dari 5 karakter',
				],
			],
			[
				'field' => 'nama_barang',
				'label'	=> 'nama Barang',
				'rules' => 'required',
				'errors' =>[
					'required' => 'nama Barang tidak Boleh Kosong',
				],

			],
			[
				'field' => 'harga_barang',
				'label'	=> 'harga Barang',
				'rules' => 'required|numeric',
				'errors' =>[
					'required' => 'harga Barang tidak Boleh Kosong',
					'numeric' => 'Harag Barang Harus Angka',
				],
			],
			[
				'field' => 'kode_barang',
				'label'	=> 'kode Barang',
				'rules' => 'required|max_length[5]',
				'errors' =>[
					'required' => 'kode Baraang tidak Boleh Kosong',
					'max_length' => 'kode Barang tdak Boleh Lebih dari 5 karakter',
				],
			],
			[
				'field' => 'kode_jenis',
				'label'	=> 'kode Jenis',
				'rules' => 'required',
				'errors' =>[
					'required' => 'kode Jenis tidak Boleh Kosong',
					
				],
			],
			[
				'field' => 'stock',
				'label'	=> 'stock',
				'rules' => 'required|numeric',
				'errors' =>[
					'required' => 'Stock tidak Boleh Kosong',
					'numeric' => 'Stock Barang Harus Angka',
				],
			]
		];
	}
	
	public function tampilDataBarang()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	}

	public function tampilDataBarang2()
	{
		//menggunakan query
		$query = $this->db->query("SELECT * FROM barang as br inner join jenis_barang as jb on br.kode_jenis=jb.kode_jenis");
		return $query->result();
	}

	public function tampilDataBarang3()
	{
		//menggunakan query 
		$this->db->select('*');
		$this->db->order_by('kode_barang', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function save()
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabungan = $thn."-".$bln."-".$tgl;
		
		$data['kode_barang']			= $this->input->post('kode_barang');
		$data['nama_barang']			= $this->input->post('nama_barang');
		$data['harga_barang']			= $this->input->post('harga_barang');
		$data['kode_jenis']				= $this->input->post('kode_jenis');
		$data['stock']           		= $this->input->post('stock');
		$data['flag']					= 1;
		$this->db->insert($this->_table, $data);
	}
	public function update($kode_barang)
	{		
		$data['kode_barang']			= $this->input->post('kode_barang');
		$data['nama_barang']			= $this->input->post('nama_barang');
		$data['harga_barang']			= $this->input->post('harga_barang');
		$data['kode_jenis']				= $this->input->post('kode_jenis');
		$data['stock']           		= $this->input->post('stock');
		$data['flag']					= 1;
		$this->db->where('kode_barang', $kode_barang);
		$this->db->update($this->_table, $data);
	}
	public function detail($kode_barang)
	{
		$this->db->select('*');
		$this->db->where('kode_barang', $kode_barang);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();	
	}
	public function editdetail($kode_barang)
	{
		$this->db->select('*');
		$this->db->where('kode_barang', $kode_barang);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();	
	}
	public function updateStok($kode_barang, $qty)
    {
        //panggil data detail stok
        $cari_stok = $this->detail($kode_barang);
        foreach ($cari_stok as $data) {
            $stok = $data->stock;
        }
        
        //proses update stok table barang
        $jumlah_stok     = $stok + $qty;
        $data_barang['stock']    = $jumlah_stok;
        
        $this->db->where('kode_barang', $kode_barang);
        $this->db->update('barang', $data_barang);
    }
    public function updateStok1($kode_barang, $qty)
    {
        //panggil data detail stok
        $cari_stok = $this->detail($kode_barang);
        foreach ($cari_stok as $data) {
            $stok = $data->stock;
        }
        
        //proses update stok table barang
        $jumlah_stok     = $stok - $qty;
        $data_barang['stock']    = $jumlah_stok;
        
        $this->db->where('kode_barang', $kode_barang);
        $this->db->update('barang', $data_barang);
    }

    public function tampilDataBarang2Pagination($perpage, $uri, $data_pencarian)
	{
		//$this->db->query("SELECT * FROM barang as br inner join jenis_barang as jb on br.kode_jenis=jb.kode_jenis order by kode_barang asc");
		$this->db->select('barang.kode_barang, barang.nama_barang, barang.harga_barang, jenis_barang.nama_jenis, barang.stock');
		//$this->db->from('barang');
		$this->db->join('jenis_barang', 'jenis_barang.kode_jenis = barang.kode_jenis');
		//$query = $this->db->get();
		if (!empty($data_pencarian)) {
			# code...
			$this->db->like('barang.nama_barang', $data_pencarian);
		}
		$this->db->order_by('barang.kode_barang', 'asc');

		$get_data = $this->db->get($this->_table,  $perpage, $uri);
		if ($get_data->num_rows()>0) {
			# code...
			return $get_data->result();
		}else{
			return null;
		}
	}
	public function tombolpagination($data_pencarian)
	{
		$this->db->like('barang.nama_barang', $data_pencarian);
		$this->db->from($this->_table);
		$hasil = $this->db->count_all_results();

		$pagination['base_url']		= base_url().'barang/listbarang/load/';
		$pagination['total_rows']	= $hasil;
		$pagination['per_page']		= "3";
		$pagination['uri_segment']	= 4;
		$pagination['num_links']	= 2;

		$pagination['full_tag_open']	= '<div class="pagination">';
		$pagination['full_tag_close']	= '</div>';

		$pagination['first_link']		= 'First Page';
		$pagination['first_tag_open']	= '<span class="firstlink">';
		$pagination['first_tag_close']	= '</span>';

		$pagination['last_link']		= 'Last Page';
		$pagination['last_tag_open']	= '<span class="lastlink">';
		$pagination['last_tag_close']	= '</span>';

		$pagination['next_link']		= 'Next Page';
		$pagination['next_tag_open']	= '<span class="nextlink">';
		$pagination['next_tag_close']	= '</span>';

		$pagination['prev_link']		= 'Prev Page';
		$pagination['prev_tag_open']	= '<span class="prevlink">';
		$pagination['prev_tag_close']	= '</span>';

		$pagination['cur_tag_open']		= '<div class="curlink">';
		$pagination['cur_tag_close']	= '</div>';

		$pagination['num_tag_open']		= '<div class="numlink">';
		$pagination['num_tag_close']	= '</div>';

		$this->pagination->initialize($pagination);

		$hasil_pagination = $this->tampilDataBarang2Pagination($pagination['per_page'],
			$this->uri->segment(4), $data_pencarian);

		return $hasil_pagination;
	}
	public function createKodeUrut()
	{
		// cek kode barang terakhir
		$this->db->select('MAX(kode_barang) as kode_barang');
		$query = $this->db->get($this->_table);
		$result = $query->row_array(); //hasil berbentuk array

		$kode_barang_terakhir = $result['kode_barang'];
		// format BR001 = BR [label] 001 [nomer urut]
		$label = 'BR';
		$no_urut = (int) substr($kode_barang_terakhir, 2, 3);
		$no_urut ++;

		$no_urut_baru = sprintf("%03s", $no_urut);
		$kode_barang_baru = $label . $no_urut_baru;

		// var_dump($kode_barang_baru); die();
		// var_dump(string("%03s", 0)); die();

		return $kode_barang_baru; 
	}
	public function cariHargaBarang($kode_barang)
    {
        $query	= $this->db->query("SELECT * FROM " . $this->_table . " WHERE flag = 1 AND kode_barang = '$kode_barang'");
        $hasil = $query->result();	

        foreach($hasil as $data) {
            $harganya = $data->harga_barang;
        }

        //yang dikirim hasil harga
        return $harganya;
    }
    public function cariNamaBarang($kode_barang)
    {
        $query	= $this->db->query("SELECT * FROM " . $this->_table . " WHERE flag = 1 AND kode_barang = '$kode_barang'");
        $hasil = $query->result();	

        foreach($hasil as $data) {
            $nama_barang = $data->nama_barang;
        }

        //yang dikirim hasil harga
        return $nama_barang;
    }

	public function delete($kode_barang)
	{
		// $this->hapusPhoto($kode_barang);
		$this->db->where('kode_barang', $kode_barang);
		$this->db->delete($this->_table);
	}
	
	
}